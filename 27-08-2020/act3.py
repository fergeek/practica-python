# from math import sqrt
# Funcion Pitagoras
def pitagoras(a, b):
    return (a**2 + b**2)**(1/2)


print(' Pitagoras '.upper().center(36, '-'))
a = float(input(' Ingrese A: '))
b = float(input(' Ingrese B: '))
print(' El lado C vale: %.2f' % (pitagoras(a, b)))
