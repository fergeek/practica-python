# Do-while y uso de sort
ret = []
print(' Ingrese palabras a ordenar '.upper().center(36, '-'))
while True:
    aux = input("> ").capitalize()
    if aux != '':
        ret.append(aux)
    else:
        break
# Mostrar palabras en orden
print(' Palabras ordenadas '.upper().center(36, '-'))
ret.sort()
for i, x in enumerate(ret, start=1):
    print(f'{i}) {x}')
