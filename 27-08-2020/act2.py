# Funcion sumatoria
def listSum(list):
    ret = 0
    for x in list:
        ret += x
    return ret


# Funcion productoria
def listProd(list):
    ret = 1
    for x in list:
        ret *= x
    return ret


ret = []
print(' Ingrese numeros a sumar '.upper().center(36, '-'))
while True:
    aux = input("> ")
    if aux != '' and aux.isnumeric():
        ret.append(float(aux))
    else:
        break
# Mostrar palabras en orden
print(' Resultados '.upper().center(36, '-'))
print(ret)
print(' Sumatoria:\t%5.3f\n Productoria:\t%5.3f' %
      (listSum(ret), listProd(ret)))
