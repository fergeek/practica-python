# Pedir un mes al usuario y clasificarlo por est

meses = {
    "Verano": [
        "diciembre",
        "enero",
        "febrero"
    ],
    "Otoño": [
        "marzo",
        "abril",
        "mayo"
    ],
    "Invierno": [
        "junio",
        "julio",
        "agosto"
    ],
    "Primavera": [
        "septiembre",
        "octubre",
        "noviembre"
    ]
}
mesI = input(" Ingrese un mes del año: ").lower()
estacion = ""
for est in meses.keys():
    if mesI in meses[est]:
        estacion = est
        break
if estacion != "":
    print(f" En {mesI} estamos en {estacion}")
else:
    print(f" No se encontro el mes {mesI}")
