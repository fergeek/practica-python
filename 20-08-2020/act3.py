def getTabla(n, t=10):
    ret = []
    n = int(n)
    for x in range(1, t+1):
        ret.append(int(x*n))
    return ret


n = input(" Ingrese la tabla que quiere mostar: ")
tabla = getTabla(n)
for x in range(1, 10):
    print(f"{x} x {n} = {tabla[x]}")
