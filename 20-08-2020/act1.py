# Conversor de temperatura de F a C y C a F
def subsNumber(string):
    ret = ""
    string.replace(',', '.')
    for x in string:
        if x in "0123456789.":
            ret += x
    return float(ret)


def isCelsius(input):
    ret = False
    if "F" in input.upper() and "C" not in input.upper():
        ret = True
    return ret


temp = ""
print("Ingrese un valor de temperatura seguido de su unidad (Celsius por"
      + "\ndefecto), para finalizar ingrese 'x':")
while temp != 'X':
    temp = input(" > ").upper()
    if temp != "X":
        val = subsNumber(temp)
        toVal = ((9*val + 35 * 5)/5, (5*(val-32))/9)[isCelsius(temp)]
        current = str(val) + ' ' + ('°C', '°F')[isCelsius(temp)]
        to = (f"{str(toVal)} °F", f"{str(toVal)} °C")[isCelsius(temp)]
        print(f" {current} >>> {to}")
