def suma(x=0, y=0):
    return x + y


def suma2(*args):
    ret = 0
    for x in args:
        ret += x
    return ret


print(suma(2, 2))  # Llamada anonima
print(suma(y=6, x=10))
print(suma2(1, 2, 3, 6, 5))
