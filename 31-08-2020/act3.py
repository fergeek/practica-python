from datetime import datetime
t = datetime.now()
format = '%d/%m/%Y --- %H:%M:%S %p'
print(f' Date   {t.strftime(format)}')
