from datetime import datetime, timedelta
t = datetime.now()
params = '01/01/2020 --- 13:00:00 AM'
format = '%d/%m/%Y --- %H:%M:%S %p'
nf = t.strptime(params, format)
print(f' Date   {nf.date()}')
fecha1 = t.strptime('31/08/2020', '%d/%m/%Y')
fecha2 = t.strptime('25/12/2020', '%d/%m/%Y')
df = fecha2 - fecha1
print(f' Falta {df.days} dias para navidad...')
delta = timedelta(days=365)
print(f' Dentro de un año sera {(fecha1+delta)}')
