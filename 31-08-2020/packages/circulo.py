from math import pi


# Func diametro'circulo.py'

def diam(r):
    return 2*float(r)


# Func perimetro
def perim(r):
    return 2*float(pi)*r


# Area
def area(r):
    return pi*(float(r)**2)


# Data
def circulo(r):
    return diam(r), perim(r), area(r)
