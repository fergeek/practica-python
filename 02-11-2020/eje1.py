from PyQt5.QtWidgets import QMainWindow, QApplication, QLineEdit, QMessageBox
from PyQt5 import uic


class MainWindow(QMainWindow):
    def __init__(self, uiFile=''):
        super().__init__()
        uic.loadUi(uiFile, self)
        self.msj = QMessageBox()
        self.submit.clicked.connect(self.run)

    def run(self):
        precio = {
            'Simple': 150,
            'Doble': 250,
            'Suite': 375
        }
        fecha = self.calendar.selectedDate()
        dias = self.days.value()
        llegada = fecha.toString('dd/MM/yyyy')
        salida = fecha.addDays(dias).toString('dd/MM/yyyy')
        tipo = self.tipoh.itemText(self.tipoh.currentIndex())
        if dias > 0:
            self.getMsj(
                'Resumen', f'Llegada: {llegada}\nSalida: {salida}\nDías: {dias}\nHabitación: {tipo}\nTotal: U$D {dias*precio[tipo]}', icon=self.msj.Information)
        else:
            self.getMsj('Error', 'La estadía mínima es de un día',
                        icon=self.msj.Warning)

    def getMsj(self, title='', txt='', icon=''):
        self.msj.setWindowTitle(title)
        self.msj.setIcon(self.msj.Warning if icon == '' else icon)
        self.msj.setText(txt)
        self.msj.exec_()


app = QApplication([])
ventana = MainWindow('estadia.ui')
ventana.show()
app.exec_()
