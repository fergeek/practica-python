from PyQt5.QtWidgets import QMainWindow, QApplication, QLineEdit, QMessageBox
from PyQt5 import uic


class MainWindow(QMainWindow):
    def __init__(self, uiFile=''):
        super().__init__()
        uic.loadUi(uiFile, self)
        self.msj = QMessageBox()
        self.actionSumar.triggered.connect(lambda: self.run('+'))
        self.actionRestar.triggered.connect(lambda: self.run('-'))
        self.actionMultiplicar.triggered.connect(lambda: self.run('*'))
        self.actionDividir.triggered.connect(lambda: self.run('/'))
        self.actionElevar.triggered.connect(lambda: self.run('^'))
        self.actionRaiz.triggered.connect(lambda: self.run('sqrt'))
        self.actionSalir.triggered.connect(lambda: self.run('x'))

    def run(self, arg):
        A = float(self.inpa.text())
        B = float(self.inpb.text())
        if arg == '+':
            ret, txt = (A + B, 'A + B')
        elif arg == '-':
            ret, txt = (A - B, 'A - B')
        elif arg == '*':
            ret, txt = (A * B, 'A * B')
        elif arg == '/':
            ret, txt = (A / B, 'A / B')
        elif arg == '^':
            ret, txt = (A ** B, 'A ^ B')
        elif arg == 'sqrt':
            ret, txt = (A ** (1/B), 'Raiz B de A')
        elif arg == 'x':
            app.quit()

        self.getMsj('Resultado', f'{txt} = {ret}')

    def getMsj(self, title='', txt='', icon=''):
        self.msj.setWindowTitle(title)
        self.msj.setIcon(self.msj.Warning if icon == '' else icon)
        self.msj.setText(txt)
        self.msj.exec_()


app = QApplication([])
ventana = MainWindow('calc.ui')
ventana.show()
app.exec_()
