from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog, QMessageBox
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje2.ui', self)
        # self.lista.currentIndexChaned.connect(self.onChanged)
        self.barProgreso.valueChanged.connect(self.onProgress)
        self.download.clicked.connect(self.downloadF)

    def onProgress(self):
        self.displayText.setText(str(self.barProgreso.value()))

    def downloadF(self):
        d = 0
        while d < 100.0:
            d += 0.00001
            self.barProgreso.setValue(d)


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
