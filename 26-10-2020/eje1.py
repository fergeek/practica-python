from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog, QMessageBox
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        # self.lista.currentIndexChaned.connect(self.onChanged)
        self.addBtn.clicked.connect(self.addEle)
        self.editBtn.clicked.connect(self.editEle)
        self.removeBtn.clicked.connect(self.rmvEle)
        self.cleanBtn.clicked.connect(self.cleanList)

    def addEle(self):
        value, ok = QInputDialog().getText(self, 'Ingresar', 'Ingrese un valor')
        if ok:
            self.lista.addItem(value)

    def editEle(self):
        index = self.lista.currentIndex()
        currentV = self.lista.currentText()
        value, ok = QInputDialog().getText(
            self, 'Editar', f'Valor actual "{currentV}"\nIngrese nuevo:')
        if ok:
            self.lista.setItemText(index, value)

    def rmvEle(self):
        msj = QMessageBox()
        msj.setWindowTitle('Borrar item')
        msj.setText(f'Desea eliminar "{self.lista.currentText()}" ?')
        msj.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        msj.setIcon(msj.Warning)
        ret = msj.exec_()
        if ret == QMessageBox.Yes:
            self.lista.removeItem(self.lista.currentIndex())

    def cleanList(self):
        ret = self.msjBox(
            'Eliminar todo', 'Se borraran todos los elementos', icon='critical')
        if ret == QMessageBox.Yes:
            self.lista.clear()

    def msjBox(title, txt, buttons='', icon='warning'):
        msj = QMessageBox()
        # msj.setWindowTitle(title)
        msj.setText(txt)
        msj.setStandardButtons(buttons if not buttons
                               == '' else QMessageBox.Yes | QMessageBox.Cancel)
        if icon.lower() in ['warning', 'information', 'critical', 'question']:
            msj.setIcon(eval(f'msj.{icon.capitalize()}'))
        else:
            msj.setIcon(msj.Question)
        return msj.exec_()
        # def onChanged(self):
        #     if self.radioInp.isChecked():
        #         value, ok = QInputDialog().getText(self, 'Ingresar', 'Ingrese un valor')
        #     if ok and value:
        #         self.input.setText(str(value))


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
