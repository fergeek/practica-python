from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje2.ui', self)
        self.radioInp_1.toggled.connect(self.onChange)
        self.radioInp_2.toggled.connect(self.onChange)
        self.radioInp_3.toggled.connect(self.onChange)

    def onChange(self):
        if self.radioInp_1.isChecked():
            ret = 'Value 1'
        elif self.radioInp_2.isChecked():
            ret = 'Value 2'
        elif self.radioInp_3.isChecked():
            ret = 'Value 3'
        self.displayText.setText(ret)


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
