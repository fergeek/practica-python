from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        self.submitButton.clicked.connect(self.onClick)

    def onClick(self):
        if self.radioInp_1.isChecked():
            ret = 'Value 1'
        elif self.radioInp_2.isChecked():
            ret = 'Value 2'
        elif self.radioInp_3.isChecked():
            ret = 'Value 3'
        self.displayText.setText(ret)


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
