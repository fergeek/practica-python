from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje3.ui', self)
        self.radioInpC1_1.toggled.connect(self.onChange)
        self.radioInpC1_2.toggled.connect(self.onChange)
        self.radioInpC1_3.toggled.connect(self.onChange)
        self.radioInpC2_1.toggled.connect(self.onChange)
        self.radioInpC2_2.toggled.connect(self.onChange)
        self.radioInpC2_3.toggled.connect(self.onChange)

    def onChange(self):
        if self.radioInpC1_1.isChecked():
            num = '1'
        elif self.radioInpC1_2.isChecked():
            num = '2'
        elif self.radioInpC1_3.isChecked():
            num = '3'
        else:
            num = '0'
        # ------------------------------
        if self.radioInpC2_1.isChecked():
            alp = 'A'
        elif self.radioInpC2_2.isChecked():
            alp = 'B'
        elif self.radioInpC2_3.isChecked():
            alp = 'C'
        else:
            alp = 'X'
        self.displayText.setText(f'Seleccion: {num}-{alp}')


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
