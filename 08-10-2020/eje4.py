from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje4.ui', self)
        self.submit.clicked.connect(self.onClick)

    def onClick(self):
        num = 0
        if self.ext1.isChecked():
            num += 20
        if self.ext2.isChecked():
            num += 50
        if self.ext3.isChecked():
            num += 10
        self.displayText.setText('Precio total: $%.2f' % (num))


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
