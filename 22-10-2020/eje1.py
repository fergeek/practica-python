from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        self.submit.clicked.connect(self.onSubmitClick)

    def onSubmitClick(self):
        # text, ok = QInputDialog().getText(self, 'Ingresar', 'Ingrese un texto')
        # if ok and text:
        #     self.input.setText(text)
        # number, ok = QInputDialog().getInt(self, 'Ingresar', 'Ingrese un valor',
        #                                    value=8, min=0, max=100, step=2)
        # if ok and number != 0:
        #     self.input.setText(str(number))
        # number, ok = QInputDialog().getDouble(
        #     self, 'Ingresar', 'Ingrese un valor decimal',
        #     value=0.45, min=0.45, max=5.25, decimals=2
        #     )
        # if ok and number != 0:
        #     self.input.setText(str(number))
        items = ['rojo', 'amarillo', 'verde']
        text, ok = QInputDialog().getItem(
            self, 'Ingresar', 'Ingrese un valor decimal',
            items, 0, False
            )
        if ok and text != 0:
            self.input.setText(str(text))


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
