from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje3.ui', self)
        self.msjButton.clicked.connect(self.onSubmitClick)

    def onSubmitClick(self):
        msj = QMessageBox()
        msj.setWindowTitle('TitleMSJ')
        msj.setText('Texto MSJ')
        msj.setIcon(msj.Warning)
        # msj.setIcon(msj.Question)
        # msj.setIcon(msj.Critical)
        # msj.setIcon(msj.Information)
        # msj.setStandardButtons(
        #     QMessageBox.Yes | QMessageBox.No | QMessageBox.Ok | QMessageBox.Cancel
        #     | QMessageBox.Save | QMessageBox.SaveAll | QMessageBox.Abort | QMessageBox.Retry
        #     | QMessageBox.Ignore
        #     )
        msj.setStandardButtons(
            QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel)
        ret = msj.exec_()
        if ret == QMessageBox.Yes:
            print('Ususario: Yes')
        elif ret == QMessageBox.No:
            print('Ususario: No')
        else:
            print('Ususario: Cancel')


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
