from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje2.ui', self)
        self.submit.clicked.connect(self.onSubmitClick)

    def onSubmitClick(self):
        if self.radioInp.isChecked():
            value, ok = QInputDialog().getText(self, 'Ingresar', 'Ingrese un valor')
        elif self.radioInp_2.isChecked():
            value, ok = QInputDialog().getInt(self, 'Ingresar', 'Ingrese un valor',
                                              value=8, min=0, max=100, step=2)
        elif self.radioInp_3.isChecked():
            value, ok = QInputDialog().getDouble(
                self, 'Ingresar', 'Ingrese un valor decimal',
                value=0.45, min=0.45, max=5.25, decimals=2
            )
        elif self.radioInp_4.isChecked():
            items = ['rojo', 'amarillo', 'verde']
            value, ok = QInputDialog().getItem(
                self, 'Ingresar', 'Ingrese un valor decimal',
                items, 0, False
            )
        if ok and value:
            self.input.setText(str(value))


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
