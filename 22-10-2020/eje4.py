from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje4.ui', self)
        self.submit.clicked.connect(self.onSubmitClick)

    def onSubmitClick(self):
        msj = QMessageBox()
        msj.setWindowTitle(self.title.text())
        msj.setText(self.msjText.text())
        # set Icon
        if self.rdi_1.isChecked():
            msji = msj.Warning
        elif self.rdi_2.isChecked():
            msji = msj.Critical
        elif self.rdi_3.isChecked():
            msji = msj.Information
        elif self.rdi_4.isChecked():
            msji = msj.Question
        else:
            msji = msj.Warning
        msj.setIcon(msji)
        # Set buttons
        buttons = ['Yes', 'No', 'Ok', 'Cancel']
        retButtons = ''
        for x in buttons:
            if eval(f'self.ok{x}.isChecked()'):
                retButtons += f' QMessageBox.{x} | '
        if not retButtons == '':
            eval(f'msj.setStandardButtons({retButtons[:-3]})')
        ret = msj.exec_()
        if ret == QMessageBox.Yes:
            print('Ususario: Yes')
        elif ret == QMessageBox.No:
            print('Ususario: No')
        else:
            print('Ususario: Cancel')


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
