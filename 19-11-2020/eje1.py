from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic
import sqlite3 as db


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('users.ui', self)
        self.data = []
        self.currentIndex = None
        self.loadUsers.clicked.connect(self.getAllUsers)
        self.btnNext.clicked.connect(self.next)
        self.btnBack.clicked.connect(self.back)
        self.btnFirst.clicked.connect(lambda: self.loadUser(0))
        self.btnLast.clicked.connect(lambda: self.loadUser(len(self.data)-1))

    def getAllUsers(self):
        main = db.connect('./database.db')
        cursor = main.cursor()
        cursor.execute('SELECT * FROM Usuarios')
        for x in cursor.fetchall():
            if x not in self.data:
                self.data.append(list(x))
        if len(self.data) > 0:
            self.loadUser(0)
            self.currentIndex = 0
        main.close()

    def loadUser(self, index):
        if self.currentIndex != None:
            if len(self.data[index]) == 5:
                self.inputId.setText(str(self.data[index][0]))
                self.inputUser.setText(str(self.data[index][1]))
                self.inputName.setText(str(self.data[index][4]))
                self.inputEmail.setText(str(self.data[index][2]))
                self.currentIndex = index
            else:
                print('LoadError: invalid args length')
        self.verifyBtns()

    def verifyBtns(self):
        if self.currentIndex is not None:
            if self.currentIndex == 0:
                self.btnFirst.setEnabled(False)
                self.btnBack.setEnabled(False)
                self.btnLast.setEnabled(True)
                self.btnNext.setEnabled(True)
            elif self.currentIndex == len(self.data) - 1:
                self.btnLast.setEnabled(False)
                self.btnNext.setEnabled(False)
                self.btnBack.setEnabled(True)
                self.btnFirst.setEnabled(True)
            else:
                self.btnLast.setEnabled(True)
                self.btnNext.setEnabled(True)
                self.btnBack.setEnabled(True)
                self.btnFirst.setEnabled(True)

    def next(self):
        n = self.currentIndex + 1
        if n in range(0, len(self.data)):
            self.currentIndex = n
            self.loadUser(self.currentIndex)

    def back(self):
        n = self.currentIndex - 1
        if n in range(0, len(self.data)):
            self.currentIndex = n
            self.loadUser(self.currentIndex)


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
