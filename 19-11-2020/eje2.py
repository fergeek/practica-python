from PyQt5.QtWidgets import QMainWindow, QApplication, QMessageBox
from PyQt5 import uic
import sqlite3 as db


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('users_save.ui', self)
        self.maindb = db.connect('./database.db')
        self.db = self.maindb.cursor()
        self.data = []
        self.currentIndex = None
        self.loadUsers.clicked.connect(self.getAllUsers)
        self.btnNext.clicked.connect(self.next)
        self.btnBack.clicked.connect(self.back)
        self.btnFirst.clicked.connect(lambda: self.loadUser(0))
        self.btnLast.clicked.connect(lambda: self.loadUser(len(self.data)-1))
        self.btnSave.clicked.connect(self.saveChangues)

    def getAllUsers(self, index=0):
        self.db.execute('SELECT * FROM Usuarios')
        for x in self.db.fetchall():
            p = self.inData(x)
            if p == -1:
                self.data.append(list(x))
        if len(self.data) > 0:
            self.loadUser(index)
            self.currentIndex = index

    def loadUser(self, index):
        if index in range(0, len(self.data)):
            self.inputId.setText(str(self.data[index][0]))
            self.inputUser.setText(str(self.data[index][1]))
            self.inputName.setText(str(self.data[index][4]))
            self.inputEmail.setText(str(self.data[index][2]))
            self.currentIndex = index
        self.verifyBtns()

    def verifyBtns(self):
        if self.currentIndex is not None:
            if self.currentIndex == 0:
                self.btnFirst.setEnabled(False)
                self.btnBack.setEnabled(False)
                self.btnLast.setEnabled(True)
                self.btnNext.setEnabled(True)
            elif self.currentIndex == len(self.data) - 1:
                self.btnLast.setEnabled(False)
                self.btnNext.setEnabled(False)
                self.btnBack.setEnabled(True)
                self.btnFirst.setEnabled(True)
            else:
                self.btnLast.setEnabled(True)
                self.btnNext.setEnabled(True)
                self.btnBack.setEnabled(True)
                self.btnFirst.setEnabled(True)

    def next(self):
        if self.currentIndex is not None:
            n = self.currentIndex + 1
            if n in range(0, len(self.data)):
                self.currentIndex = n
                self.loadUser(self.currentIndex)

    def back(self):
        if self.currentIndex is not None:
            n = self.currentIndex - 1
            if n in range(0, len(self.data)):
                self.currentIndex = n
                self.loadUser(self.currentIndex)

    def saveChangues(self):
        self.db.execute(f'''UPDATE Usuarios SET
         nombre = \'{self.inputName.text()}\',
         nick = \'{self.inputUser.text()}\',
         mail = \'{self.inputEmail.text()}\'
         WHERE id = {self.inputId.text()};
        ''')
        self.maindb.commit()
        self.getAllUsers(self.currentIndex)
        self.msjBox('Se actualizó el registro',
                    QMessageBox.Ok, icon='infomation')

    def msjBox(title, txt, buttons='', icon='warning'):
        msj = QMessageBox()
        # msj.setWindowTitle(title)
        msj.setText(txt)
        msj.setStandardButtons(buttons if not buttons
                               == '' else QMessageBox.Yes | QMessageBox.Cancel)
        if icon.lower() in ['warning', 'information', 'critical', 'question']:
            msj.setIcon(eval(f'msj.{icon.capitalize()}'))
        else:
            msj.setIcon(msj.Question)
        return msj.exec_()

    def inData(self, ele):
        ret = -1
        for i, x in enumerate(self.data):
            if '..'.join(str(v) for v in ele) == '..'.join(str(v) for v in x):
                ret = i
                break
        return ret


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
