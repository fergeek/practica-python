from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje3.ui', self)
        self.alista.itemSelectionChanged.connect(self.itemOnChange)

    def itemOnChange(self):
        self.blista.clear()
        for x in list(self.alista.selectedItems()):
            self.blista.addItem(x.text())


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
