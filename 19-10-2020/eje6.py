from PyQt5.QtWidgets import QMainWindow, QApplication, QInputDialog
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje6.ui', self)
        self.submit.clicked.connect(self.addItem)
        self.remove.clicked.connect(self.removeF)
        self.editar.clicked.connect(self.editarF)
        self.lista.doubleClicked.connect(self.editarF)
        self.removeAll.clicked.connect(self.removeAllF)

    def addItem(self):
        self.lista.addItem(self.input.text())
        self.input.clear()

    def editarF(self):
        newText, ok = QInputDialog().getText(self, 'Editar', 'Ingrese nuevo texto')
        if ok and newText != '':
            self.lista.currentItem().setText(newText)

    def removeF(self):
        self.lista.takeItem(self.lista.currentRow())

    def removeAllF(self):
        self.lista.clear()


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
