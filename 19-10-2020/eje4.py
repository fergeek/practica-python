from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje4.ui', self)
        self.submit.clicked.connect(self.addItem)

    def addItem(self):
        self.lista.addItem(self.input.text())
        self.input.clear()


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
