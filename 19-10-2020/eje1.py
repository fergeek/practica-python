from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        self.lista.itemClicked.connect(self.itemOnClick)

    def itemOnClick(self):
        self.displayText.setText(self.lista.currentItem().text())


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
