from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje5.ui', self)
        self.submit.clicked.connect(self.addItem)
        self.remove.clicked.connect(self.removeF)
        self.removeAll.clicked.connect(self.removeAllF)

    def addItem(self):
        self.lista.addItem(self.input.text())
        self.input.clear()

    def removeF(self):
        self.lista.takeItem(self.lista.currentRow())

    def removeAllF(self):
        self.lista.clear()


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
