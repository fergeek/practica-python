from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        # self.lista.itemSelectionChange.connect(self.itemOnClick)
        self.lista.currentItemChanged.connect(self.itemOnChangue)

    def itemOnClick(self):
        self.displayText.setText(self.lista.currentItem().text())

    def itemOnChangue(self, current, prev):
        self.displayText.setText(current.text())
        if prev:
            print(f'Anterior: {prev.text()}')


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
