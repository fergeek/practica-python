#Tipos de Variables
int = 10
float = 1.333
cadena = "Hello World!!"
bool = True
# Imprimir (1)
print(" Entero: ", int, "\n")
# Imprimir (2)
print(" Entero = {0}\n Float = {1}\n String = {2}\n Boolean = {3}\n".format(
   int,
   float,
   cadena,
   bool
))
# Imprimir (3)
print(f"\n Int: {int}\n Float: {float}\n String: {cadena}\n Bool: {bool}")
