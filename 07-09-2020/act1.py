from package.musica import *
ListaCancs = Canciones()
# Vector de carga
cancs = [
    ['Evergreen', 'Broods', 301, 'Alternative'],
    ['L.A.F', 'Broods', 205, 'Alternative'],
    ['Couldn\'t Believe', 'Broods', 123, 'Alternative'],
    ['Killing You', 'Broods', 301, 'Alternative'],
    ['The Four Seasons', 'Antonio Vivaldi', 240, 'Clasica']
]
# Cargar Canciones
for x in cancs:
    ListaCancs.append(Cancion(*x))

print('Todas las canciones')
ListaCancs.listar()
print('Genero: Alternative')
ListaCancs.listar(by='categoria', searchArg='Alternative')
print('Interprete: Antotnio Vivaldi')
ListaCancs.listar(by='autor', searchArg='Antonio Vivaldi')
ListaCancs.remove('L.A.F', 'Broods')
print('Sin L.A.F')
ListaCancs.listar()
