class Cancion:
    def __init__(self, title, author, duration, cat):
        self.title = title
        self.author = author
        self.duration = duration
        self.cat = cat

    def resumen(self):
        return f'{self.title}, {self.author}'

    def __str__(self):
        return f'{self.resumen()} [{self.duration}seg] ({self.cat})'


class Canciones:
    def __init__(self, *args):
        self.lista = []
        for x in args:
            if type(x) == Cancion:
                self.lista.append(x)

    def append(self, cancion):
        if type(cancion) == Cancion:
            self.lista.append(cancion)

    def find(self, titulo, interp=None):
        ret = -1
        for i, x in enumerate(self.lista):
            if interp is None:
                if titulo.lower() == x.title.lower():
                    ret = i
            else:
                if titulo.lower() == x.title.lower() and interp.lower() == x.author.lower():
                    ret = i
        return ret

    def remove(self, titulo, interprete):
        p = self.find(titulo, interprete)
        if p != -1:
            del self.lista[p]
        return p != -1

    def searchBy(self, by, searchArg):
        ret = []
        for x in self.lista:
            if by.lower() in ('author', 'autor', 'interprete'):
                if x.author == searchArg:
                    ret.append(x)
            elif by.lower() in ('categoria', 'car', 'genero', 'gen'):
                if x.cat == searchArg:
                    ret.append(x)
            elif by.lower() in ('title', 'titulo', 'nombre'):
                if x.title == searchArg:
                    ret.append(x)
        return ret

    def listar(self, by=None, searchArg=None):
        if by is None and searchArg is None:
            list = self.lista
        else:
            list = self.searchBy(by=by, searchArg=searchArg)
        for i, x in enumerate(list):
            print(f'{i+1}) {x}')
