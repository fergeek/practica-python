from functools import reduce
num = [10, 20, 30, 40, 50, 60, 71]
sumat = reduce(lambda x, y: x + y, num)  # Sumatoria
prodc = reduce(lambda x, y: x * y, num)  # Productoria
print(f'Sumatoria: {sumat}\nProductoria: {prodc}')
