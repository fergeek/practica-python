from math import pi
ang = [90, 180, 270, 45, 360]
angRad = list(map(lambda x: (x*pi) / 180, ang))
print(f'Angulos: {ang}\nEn radianes: {angRad}')
