lista = [12, 56, 89, 23, 65, 9, 12, 71, 15, 36, 48, 25]
mayores = list(filter(lambda x: x >= 18, lista))
menores = list(filter(lambda x: x < 18, lista))
print(f'Mayores {mayores}\nMenores {menores}')


def imp(x):
    return x % 2 == 1


pares = list(filter(lambda x: x % 2 == 0, lista))
# impares = filter(lambda x: x % 2 == 1, lista)
impares = list(filter(imp, lista))
print(f'Pares = {pares}\n Impares: {impares}')
