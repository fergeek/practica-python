from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('calendar.ui', self)
        self.calendario.selectionChanged.connect(self.onChange)
        self.onChange()

    def onChange(self):
        self.fecha.setDate(self.calendario.selectedDate())


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
