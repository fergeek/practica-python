from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import QtCore
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('reloj.ui', self)
        timer = QtCore.QTimer(self)
        timer.timeout.connect(self.updateClock)
        timer.start(1000)
        self.updateClock()

    def updateClock(self):
        crrntTime = QtCore.QTime.currentTime()
        timeText = crrntTime.toString('hh:mm:ss')
        self.hora.display(timeText)


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
