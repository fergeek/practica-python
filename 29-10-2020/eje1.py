from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('conv.ui', self)
        self.submit.clicked.connect(self.convert)

    def convert(self):
        inp = self.input.text()
        if self.rbDec.isChecked():
            d = 10
        elif self.rbOct.isChecked():
            d = 8
        elif self.rbHex.isChecked():
            d = 16
        elif self.rbBin.isChecked():
            d = 2

        if self.validate(inp, d):
            num = int(self.input.text(), d)
            self.lcdDec.display(num)
            self.lcdBin.display(num)
            self.lcdOct.display(num)
            self.lcdHex.display(num)
        else:
            print('Error de parametro')

    def validate(num, base):
        num = str(num)
        ret = False
        basel = {'2': '01', '16': '0123456789ABCDEF',
                 '8': '01234567', '10': '0123456789'}
        if base in (2, 10, 8, 16):
            ret = num.upper() in basel[str(base)]
        return ret


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
