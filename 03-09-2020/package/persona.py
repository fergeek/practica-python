from random import randint


def capAll(str):
    ret = ''
    for x in str.split(' '):
        ret += x.capitalize() + ' '
    return ret[:-1]


def randomEle(list):
    return list[randint(0, len(list)-1)]


def Edad(pers):
    return int(pers.edad) if type(pers) == Persona else 0


def Nombre(pers):
    return pers.nombre if type(pers) == Persona else ''


def Apellido(pers):
    return pers.apellido if type(pers) == Persona else ''


def Dni(pers):
    return int(pers.dni) if type(pers) == Persona else 0


def getRandomPersona(n=1):
    # data = dumps(open('./nombres.json', encoding='utf-8').read())
    data = {
          "nom": {
              "m": ["juana", "lucia", "lorena", "maria", "alejandra", "belen",
                    "julia", "aymara"],
              "f": ["fernando", "gaston", "luciano", "lucas", "ismael",
                    "alejandro", "julian", "enzo", ""]
                  },
          "ape": ["perez", "lopez", "alaniz", "olivera", "vargas", "torres",
                  "luna", "uribe", "perera", "pereira", "garcia", "pedraza",
                  "Jaguergui", "balardini", "santamarina"]
      }
    ret = []
    for x in range(n):
        sexo = randomEle(list(data['nom'].keys()))
        ret.append(Persona(randomEle(data['nom'][sexo]), randomEle(
            data['ape']), randint(10, 90), randint(6000000, 40000000)))
    return ret[0] if len(ret) == 1 else ret


class Persona:
    def __init__(self, nombre, apellido, edad, dni):
        self.nombre = capAll(nombre)
        self.apellido = capAll(apellido)
        self.edad = edad
        self.dni = dni

    def nombreCompleto(self):
        return f'{self.apellido}, {self.nombre}'

    def descripcion(self):
        return self.nombreCompleto() + f' DNI({self.dni}) [{self.edad}]'

    def getAllInfo(self):
        return (self.nombre, self.apellido, self.edad, self.dni)

    def __str__(self):
        return self.descripcion()


class Personas:
    def __init__(self, *args):
        self.lista = []
        for x in args:
            if type(x) == Persona:
                self.lista.append(x)

    def append(self, pers):
        ret = False
        if type(pers) == Persona:
            self.lista.append(pers)
            ret = True
        return ret

    def sortBy(self, key, reverse=False):
        if key.lower() == 'edad':
            self.lista.sort(key=Edad, reverse=reverse)
        elif key.lower() == 'nombre':
            self.lista.sort(key=Nombre, reverse=reverse)
        elif key.lower() == 'apellido':
            self.lista.sort(key=Apellido, reverse=reverse)
        elif key.lower() == 'dni':
            self.lista.sort(key=Dni, reverse=reverse)
        return self.lista
