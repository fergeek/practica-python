from math import pi


class Circulo:
    def __init__(self, r):
        self.r = r

    def setRadius(self, r):
        self.r = r

    def diam(self, other=None):
        rad = self.r if (other == None) else other
        return 2*float(rad)

    def perim(self, other=None):
        rad = self.r if (other == None) else other
        return 2*float(pi)*rad

    def area(self, other=None):
        rad = self.r if (other == None) else other
        return pi*(float(rad)**2)


class Rectangulo:
    def __init__(self, base, altura):
        self.base = base
        self.altura = altura

    def area(self):
        return self.base * self.altura

    def perim(self):
        return 2*self.base + 2*self.altura
