from package.figuras import Rectangulo
from mgprint import *
printHead('calcular rectangulo')
rec = Rectangulo(float(input(mstr('&y*& Ingrese un base: %;g%'))),
                 float(input(mstr('&y*& Ingrese un altura: %;g%'))))
mreset()
mprint(' &*_&Perimetro&;&\t%.2f u\n &*_&Area&;&\t\t%.2f u2' %
       (rec.perim(), rec.area()))
