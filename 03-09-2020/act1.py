from mgprint import *
from package.figuras import Circulo
printHead('calcular circunsferencia')
rad = float(input(mstr('&y*& Ingrese un radio: %;y%')))
mreset()
circ = Circulo(rad)
mprint(' &*_&Perimetro:&;&\t%.2f u\n &*_&Diametro:&;&\t%.2f u\n &*_&Area:&;&\t\t%.2f u2' % (
    circ.perim(), circ.diam(), circ.area()))
