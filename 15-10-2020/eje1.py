from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        self.cantLibro.valueChanged.connect(self.cLibro)
        self.cantPan.valueChanged.connect(self.cPan)

    def cLibro(self):
        self.displayLibro.setText(f'${self.getPrecioLibro()}')
        self.setTotal()

    def cPan(self):
        self.displayPan.setText(f'${self.getPrecioPan()}')
        self.setTotal()

    def getPrecioLibro(self):
        print(self.precioLibro.text())
        precio = self.fixStr(self.precioLibro.text())
        up = self.cantLibro.value()
        return precio*up

    def getPrecioPan(self):
        print(self.precioPan.text())
        precio = self.fixStr(self.precioPan.text())
        up = self.cantPan.value()
        return precio*up

    def fixStr(self, value):
        if type(value) == str and value == '':
            value = '0'
        return float(value)

    def setTotal(self):
        self.textDisplay.setText(
            f'${str(self.getPrecioPan()+self.getPrecioLibro())}')


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
