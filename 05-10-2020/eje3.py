from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje3.ui', self)
        self.btn1.clicked.connect(self.toggleBtn)
        self.btn2.clicked.connect(self.toggleBtn)

    def toggleBtn(self):
        self.btn1.setEnabled(not self.btn1.isEnabled())
        self.btn2.setEnabled(not self.btn2.isEnabled())


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
