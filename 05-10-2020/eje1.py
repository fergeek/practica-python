from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje1.ui', self)
        self.boton1.clicked.connect(self.onClick_btn1)

    def onClick_btn1(self):
        self.label1.setText('default2')


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
