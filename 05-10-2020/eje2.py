from PyQt5.QtWidgets import QMainWindow, QApplication
from PyQt5 import uic


class MyApp(QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('eje2.ui', self)
        self.setButton.clicked.connect(self.onClick_setButton)

    def onClick_setButton(self):
        self.displayText.setText(self.inputText.text())


app = QApplication([])
ventana = MyApp()
ventana.show()
app.exec_()
